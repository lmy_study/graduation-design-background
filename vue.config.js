module.exports = {
    lintOnSave: false, // 取消ESlint规范检测 不记得了吗？
    devServer: {
      proxy: { // 跨域请求
        '/api': {
          target: 'http://121.4.181.55/index.php/admin/api/',
          changeOrigin: true,
          pathRewrite: {
            '^/api': '/'
          }
        }
      }
    },
    publicPath: process.env.NODE_ENV === 'production'
      ? './' // 打包后发布文件名
      : './' // 开发环境相对路径
  
  }
  