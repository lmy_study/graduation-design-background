import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home/Home'
import Login from '@/views/Login/Login'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta:{
      isLogin:true    // 添加该字段，表示进入这个路由是需要登录的
    }
  },
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
function isNull(arg1)
{
 return !arg1 && arg1!==0 && typeof arg1!=="boolean"?true:false;
}
router.beforeEach((to,from,next)=>{
  if(to.matched.some(res=>res.meta.isLogin)){//判断是否需要登录
      var token = localStorage.getItem('admin_token')
      if (token == '' || isNull(token)) {
          next({
              path:"/",
              query:{
                  redirect:to.fullPath
              }
          });
      }else{
          next();
      }
  }else{
      next()
  }
});
export default router
